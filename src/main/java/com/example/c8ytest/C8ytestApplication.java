package com.example.c8ytest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class C8ytestApplication {

	public static void main(String[] args) {
		SpringApplication.run(C8ytestApplication.class, args);
	}
}
