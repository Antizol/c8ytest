# C8Y docker test images
FROM debian
MAINTAINER antizol@gmail.com

RUN apt-get update 
RUN apt-get install -y openjdk-11-jre openjdk-11-jdk
RUN apt-get install -y maven 
CMD ["echo", "Image created"] 